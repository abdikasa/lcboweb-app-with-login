package sheridan;

public class LoginValidator {

  public static boolean isValidLoginName(String loginName) {
    boolean valid = loginName.matches("[A-Za-z0-9]+");
    boolean doesBeginWithNumber = Character.isDigit(loginName.charAt(0));

    int count = 0;

    for (int i = 0; i < loginName.length(); i++) {
      char ch = loginName.charAt(0);
      if (isAlphaNumeric(ch)) {
        count++;
      }
    }

    if (valid && (count >= 6) && !doesBeginWithNumber) {
      return true;
    }
    throw new StringIndexOutOfBoundsException("String starts with a number OR there isn't enough characters OR not all characters are alphanumeric");
  }

  public static boolean isAlphaNumeric(char char1) {
    return (char1 >= 'a' && char1 <= 'z') || (char1 >= 'A' && char1 <= 'Z') || (char1 >= '0' && char1 <= '9');
  }
}