package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular() {
		boolean loginName = LoginValidator.isValidLoginName("thisshouldpast");
		assertTrue("this is a valid password", loginName == true);
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLoginExceptional() {
		boolean loginName = LoginValidator.isValidLoginName("3");
		assertFalse("this is not a valid password", loginName);
	}
	
	@Test
	public void testIsValidLoginBoundaryIn() {
		boolean loginName = LoginValidator.isValidLoginName("abdikasa");
		assertTrue("this is a valid password", loginName);
	}
	
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLoginBoundaryOut() {
		boolean loginName = LoginValidator.isValidLoginName("abdik");
		assertFalse("invalid password, test fails", loginName);
	}
	
	
	@Test
	public void testIsValidLoginHasNumbersBoundaryIn() {
		boolean loginName = LoginValidator.isValidLoginName("fromtoronto666god");
		assertTrue("this is a valid password", loginName);
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLoginStartNumberBoundaryOut() {
		boolean loginName = LoginValidator.isValidLoginName("3abdika");
		assertFalse("this is a valid password", loginName);
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLoginMissingCharsBoundaryOut() {
		boolean loginName = LoginValidator.isValidLoginName("abd");
		assertFalse("this is an invalid password", loginName);
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLoginInvalidCharsBoundaryOut() {
		boolean loginName = LoginValidator.isValidLoginName("abdi#");
		System.out.println(loginName);
		assertFalse("this is a invalid password", loginName);
	}

}
